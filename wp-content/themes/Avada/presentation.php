<?php
// Template Name: Presentation Log
get_header(); ?>
	<div id="content" class="full-width portfolio portfolio-one">
				<?php while(have_posts()): the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<div class="post-content">
						<?php the_content(); ?>
						<?php wp_link_pages(); ?>
					</div>
					<?php if($data['comments_pages']): ?>
						<?php wp_reset_query(); ?>
						<?php comments_template(); ?>
					<?php endif; ?>
				</div>
				<?php endwhile; ?>
	
		<?php
		$presentation_category = get_terms('presentation_category');
		if($presentation_category):
		?>
		<ul class="portfolio-tabs clearfix">
			<li class="active"><a data-filter="*" href="#"><?php echo __('All', 'Avada'); ?></a></li>
			<?php foreach($presentation_category as $presentation_cat): ?>
			<?php if(get_post_meta(get_the_ID(), 'pyre_presentation_category', true)  && !in_array('0', get_post_meta(get_the_ID(), 'pyre_presentation_category', true))): ?>
			<?php if(in_array($presentation_cat->term_id, get_post_meta(get_the_ID(), 'pyre_presentation_category', true))): ?>
			<li><a data-filter=".<?php echo $presentation_cat->slug; ?>" href="#"><?php echo $presentation_cat->name; ?></a></li>
			<?php endif; ?>
			<?php else: ?>
			<li><a data-filter=".<?php echo $presentation_cat->slug; ?>" href="#"><?php echo $presentation_cat->name; ?></a></li>
			<?php endif; ?>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		<div class="portfolio-wrapper">
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'post_type' => 'my_presentations',
				'paged' => $paged,
				'posts_per_page' => $data['presentation_items'],
			);
			$pcats = get_post_meta(get_the_ID(), 'pyre_presentation_category', true);
			if($pcats && $pcats[0] == 0) {
				unset($pcats[0]);
			}
			if($pcats){
				$args['tax_query'][] = array(
					'taxonomy' => 'presentation_category',
					'field' => 'ID',
					'terms' => $pcats
				);
			}
			$gallery = new WP_Query($args);
			while($gallery->have_posts()): $gallery->the_post();
				if(has_post_thumbnail() || get_post_meta($post->ID, 'pyre_video', true)):
			?>
			<?php
			$item_classes = '';
			$item_cats = get_the_terms($post->ID, 'presentation_category');
			if($item_cats):
			foreach($item_cats as $item_cat) {
				$item_classes .= $item_cat->slug . ' ';
			}
			endif;
			?>
			<div class="portfolio-item <?php echo $item_classes; ?>">
				<?php if(has_post_thumbnail()): ?>
				<div class="image">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('portfolio-one'); ?>
					</a>
					<div class="image-extras">
						<div class="image-extras-content">
							<?php $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
							<a class="icon" href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/link-ico.png" alt="<?php the_title(); ?>"/></a>
							<?php
							if(get_post_meta($post->ID, 'pyre_video_url', true)) {
								$full_image[0] = get_post_meta($post->ID, 'pyre_video_url', true);
							}
							?>
							<a class="icon" href="<?php echo $full_image[0]; ?>" rel="prettyPhoto[gallery]"><img src="<?php bloginfo('template_directory'); ?>/images/finder-ico.png" alt="<?php the_title(); ?>" /></a>
							<h3><?php the_title(); ?></h3>
						</div>
					</div>
				</div>
				<?php elseif(!has_post_thumbnail() && get_post_meta($post->ID, 'pyre_video', true)): ?>
				<div class="image video">
					<?php echo get_post_meta($post->ID, 'pyre_video', true); ?>
				</div>
				<?php endif; ?>
				<div class="portfolio-content">
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<h4><?php echo get_the_term_list($post->ID, 'presentation_category', '', ', ', ''); ?></h4>
					<?php $stripped_content = strip_shortcodes( tf_content( $data['excerpt_length_portfolio'] ) );
					echo $stripped_content; ?>

					<div class="buttons">
						<a href="<?php the_permalink(); ?>" class="green button small"><?php echo __('Learn More', 'Avada'); ?></a>
						<?php if(get_post_meta($post->ID, 'pyre_project_url', true)): ?>
						<a href="<?php echo get_post_meta($post->ID, 'pyre_project_url', true); ?>" class="green button small"><?php echo __('View Project', 'Avada'); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; endwhile; ?>
		</div>
		<?php themefusion_pagination($gallery->max_num_pages, $range = 2); ?>
	</div>
<?php get_footer(); ?>