<?php
// Template Name: Meeting Log
get_header(); ?>
	<?php
	if($data['blog_full_width']) {
		$content_css = 'width:100%';
		$sidebar_css = 'display:none';
	} elseif($data['blog_sidebar_position'] == 'Left') {
		$content_css = 'float:right;';
		$sidebar_css = 'float:left;';
	} elseif($data['blog_sidebar_position'] == 'Right') {
		$content_css = 'float:left;';
		$sidebar_css = 'float:right;';
	}
	?>

	<div id="content" style="<?php echo $content_css; ?>">

	<?php while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<!--	<?php global $data; if($data['featured_images'] && has_post_thumbnail()): ?>
			<div class="image">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('blog-medium'); ?>
					<div class="image-extras">
						<div class="image-extras-content">
							<img src="<?php bloginfo('template_directory'); ?>/images/link-ico.png" alt="<?php the_title(); ?>"/>
							<h3><?php the_title(); ?></h3>
						</div>
					</div>
				</a>
			</div>
			<?php endif; ?> -->
			<div class="post-content">
				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
			</div>
			<?php if($data['comments_pages']): ?>
				<?php wp_reset_query(); ?>
				<?php comments_template(); ?>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>


<?php $args = array( 'post_type' => 'my_presentations', 'posts_per_page' => 10 ); ?>
<?php $loop = new WP_Query( $args ); ?>


		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
		

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php
			if($data['featured_images']):
			if($data['legacy_posts_slideshow']) {
				include('legacy-slideshow.php');
			} else {
				include('new-slideshow.php');
			}
			endif;
			?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><h5><?php the_time($data['date_format']); ?></h5></h2>
			<div class="post-content">
				<?php
				if($data['content_length'] == 'Excerpt') {
					$stripped_content = strip_shortcodes( strip_tags( tf_content( $data['excerpt_length_blog']  ) ) );
					echo $stripped_content; 
				} else {
					the_content('');
				}
				?>
			</div>
			
		



		</div>
		<div class="clearfix"></div>
		<?php endwhile; ?>
		<?php themefusion_pagination($pages = '', $range = 2); ?>
	</div>
	<?php wp_reset_query(); ?>
	<div id="sidebar" style="<?php echo $sidebar_css; ?>">
		<?php
		if(is_home()) {
			$name = get_post_meta(get_option('page_for_posts'), 'sbg_selected_sidebar_replacement', true);
			if($name) {
				generated_dynamic_sidebar($name[0]);
			}
		} else {
			if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')): 
			endif;
		}
		?>
	</div>


		<?php wp_reset_query(); ?>

	






<?php get_footer(); ?>
