<?php
// Custom password text

function my_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    ' . __( "This presentation is password protected. Please email contact@chiari-research.org if you would like to gain access." ) . '
    <label for="' . $label . '">' . __( "Password:" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" /><input type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" />
    </form>
    ';
    return $o;
}
add_filter( 'the_password_form', 'my_password_form' );


function the_title_trim($title) {

	$title = attribute_escape($title);

	$findthese = array(
		'#Protected:#',
		'#Private:#'
	);

	$replacewith = array(
		'', // What to replace "Protected:" with
		'' // What to replace "Private:" with
	);

	$title = preg_replace($findthese, $replacewith, $title);
	return $title;
}
add_filter('the_title', 'the_title_trim');

// Register custom post types

	register_post_type(
		'presentations',
		array(
			'labels' => array(
				'name' => 'Presentations',
				'singular_name' => 'Presentation'
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'meetings'),
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail','comments'),
			'can_export' => true,
		)
	);

	register_taxonomy('presentation_category', 'presentations', array('hierarchical' => true, 'label' => 'Categories', 'query_var' => true, 'rewrite' => true));
	register_taxonomy('presentation_tools', 'presentations', array('hierarchical' => false, 'label' => 'Tools', 'query_var' => true, 'rewrite' => true));

	register_taxonomy('project_tools', 'avada_portfolio', array('hierarchical' => true, 'label' => 'Tools', 'query_var' => true, 'rewrite' => true));
// Add headshot image size	
	
	add_image_size( 'headshot', 200, 272, true ); // 220 pixels wide by 180 pixels tall, soft proportional crop mode
