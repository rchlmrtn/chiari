<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ccrc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '34a|k18bf%w{<@$-Rln?KN8a5nVK?rGSe}t|Zm?W`C(PbeUf9d;*EI-J=b)v)@g3');
define('SECURE_AUTH_KEY',  'B@MsQ=,fOX[hGv})klZ.1)`Z)|J>)ceOc9pulJBX:sC5|`-S@OZsH4T^<v7yw9i}');
define('LOGGED_IN_KEY',    '2X(~wo<v|-pe5YI{=UFKVPEvfd!6rE8StJ&.4IY&IJ*!7R$F#z*SR4dOG6wqOA:k');
define('NONCE_KEY',        'qt_| rQ+ !p8zgh-gSoH:<+Cd?PND;HgSj|DYCc]WFnr^-#8iMr6DlcL=X}Egf7j');
define('AUTH_SALT',        '*y7hc4wJ{KR;dY(|%Q8`sT(HL)}[v-K R@MPfPoF 6DmD[Q-Ljl.S}6*G&taGR~M');
define('SECURE_AUTH_SALT', '20k<Mu1knYeann9frN}#G-|T|+,H}&o;`+Wu,/6Am}Sbv}vZd/~Io?gY9Hw>,bOV');
define('LOGGED_IN_SALT',   '))2Dn8FG)<KBOdQb#+rl/%vr -%ChWc3?|vThe;^g-jFgggaOL>Pp?QfKNi]+sI{');
define('NONCE_SALT',       'mBFXe2!oA%yxScM*#+V7gi$$dhv6nO(5>J&Q__{1B/DlL; sN!r///OH8y>a_.^8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
